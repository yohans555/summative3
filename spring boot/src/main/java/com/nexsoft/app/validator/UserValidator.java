package com.nexsoft.app.validator;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.nexsoft.app.model.User;
import com.nexsoft.app.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
 
         //Regular Expression for phone
         String regexphone = "\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}";
         //Compile regular expression to get the pattern  
         Pattern patternphone = Pattern.compile(regexphone); 
         Matcher matcherphone = patternphone.matcher(user.getPhone());
         //validate phone throw error if false
         if (matcherphone.matches()==false)
             errors.rejectValue("phone", "Pattern.phone.notvalid");


        //check if username is already exist (phone and email)
        if (userService.findByEmail(user.getEmail()) != null || userService.findByPhone(user.getPhone())!=null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        //reject empty password
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");

        //validate password lenghth
        if (user.getPassword().length() < 6 || user.getPassword().length() > 12) {
            errors.rejectValue("password", "Size.userForm.password");
        }
         //Validating password with regex
         //Regular Expression for password
         String regex= "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*](?=\\S+$).{6,12}$" ; 
         //Compile regular expression to get the pattern  
         Pattern pattern = Pattern.compile(regex); 
         Matcher matcher = pattern.matcher(user.getPassword());
         //Throw error if email isnt match
         if (matcher.matches()==false){
             errors.rejectValue("password", "Pattern.password.notvalid");
         }

        //validate Re-typing password
        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
    }
}