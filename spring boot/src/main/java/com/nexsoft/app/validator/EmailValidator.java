package com.nexsoft.app.validator;



import java.util.regex.*;

import com.nexsoft.app.model.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class EmailValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        String email = user.getEmail();

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");

        //Regular Expression   
        String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";  
        //Compile regular expression to get the pattern  
        Pattern pattern = Pattern.compile(regex); 
        Matcher matcher = pattern.matcher(email);

        if (matcher.matches()==false)
            errors.rejectValue("email", "Pattern.email.notvalid");
        }
    }