package com.nexsoft.app.validator;



import com.nexsoft.app.model.User;
import com.nexsoft.app.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class LoginValidator implements Validator {
    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        System.out.println(userService.findByEmail(user.getUsername()));

        if ( userService.findByEmail(user.getUsername())==null && userService.findByPhone(user.getUsername())==null) {
            errors.rejectValue("email", "Wrong.userForm.username");
        }

        // store the password
        String currpass= user.getPassword();
        
        if(user.getUsername().matches(".*@.*")){
            String realpass =userService.findByEmail(user.getUsername()).getPassword();
            if (!currpass.equals(realpass)) {
                errors.rejectValue("password", "password incorect");
            }
        }else{
            String realpass =userService.findByPhone(user.getUsername()).getPassword();
            if (!currpass.equals(realpass)) {
                errors.rejectValue("password", "password incorect");
            }
        }

    }
}