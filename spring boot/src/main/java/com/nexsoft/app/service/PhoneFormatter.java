package com.nexsoft.app.service;

public class PhoneFormatter {
    public static String zeroDeleter(String phone){
        if((phone.substring(0,1).equalsIgnoreCase("0"))){
            String temp= phone.substring(1, phone.length());
            System.out.println("loop");
            return zeroDeleter(temp);
        }else return phone;
    }

    public static String regex(String phone,String regexphone){
        if (!(phone.substring(0,1).equalsIgnoreCase("+"))) {
            String countrycode = "+62";
            regexphone = countrycode.concat(zeroDeleter(phone));
            return regexphone;
        } else {
            regexphone=phone.replaceAll("[- ]+", "");
            return regexphone;
        }
        }
}
