package com.nexsoft.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexsoft.app.model.User;
import com.nexsoft.app.repo.UserRepository;

@Service
public class UserService{
    @Autowired
    private UserRepository userRepository;

    public void save(User user) {
        String phone = user.getPhone();
        String regexphone=null;
        user.setPhone(PhoneFormatter.regex(phone, regexphone));
        userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }
    public User findByPhone(String phone){
        return userRepository.findByPhone(phone);
    }
}
