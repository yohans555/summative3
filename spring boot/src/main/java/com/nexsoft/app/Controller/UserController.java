package com.nexsoft.app.Controller;

import java.util.ArrayList;
import java.util.List;
import com.nexsoft.app.model.User;
import com.nexsoft.app.service.UserService;
import com.nexsoft.app.validator.EmailValidator;
import com.nexsoft.app.validator.LoginValidator;
import com.nexsoft.app.validator.UserValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private LoginValidator loginValidator;


    @GetMapping(value="/registration")
    public ModelAndView registration() {
        ModelAndView mv = new ModelAndView("registration");
        mv.addObject("user", new User());
        return mv;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView registration(@ModelAttribute("user") User userForm, BindingResult bindingResult,Model model) {
        
        userForm.setUsername(userForm.getEmail());
        emailValidator.validate(userForm, bindingResult);
        userValidator.validate(userForm, bindingResult);
        
        if (bindingResult.hasErrors()) {
            ModelAndView mv = new ModelAndView("registration");
            List<String> errors = new ArrayList<>();
            for (Object object : bindingResult.getAllErrors()) {
                if(object instanceof FieldError) {
                    FieldError fieldError = (FieldError) object;
                    errors.add(fieldError.getCode());
                }
            }
            mv.addObject("errors", errors);
            return mv;
        }else{
            userService.save(userForm);
            ModelAndView mv = new ModelAndView("welcome");
            return mv;
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("user") User loginForm, BindingResult bindingResult,Model model) {
        loginValidator.validate(loginForm, bindingResult);
        if (bindingResult.getErrorCount() !=0){
            model.addAttribute("error", "Your username and password are invalid.");
            return "login";
        }else {
            return "welcome";
        }
        
    }   

    @GetMapping(value="/welcome")
    public ModelAndView welcome() {
        ModelAndView mv = new ModelAndView("welcome");
        return mv;
    }

    @GetMapping(value="/loginform")
    public ModelAndView loginForm() {
        ModelAndView mv = new ModelAndView("login");
        mv.addObject("user", new User());
        return mv;
    }
}