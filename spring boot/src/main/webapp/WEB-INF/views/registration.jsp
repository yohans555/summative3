<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Registration</h1> 
	<form:form action = "/save" modelAttribute="user">
		First Name: <form:input path="firstname"/><br/>
		Last Name: <form:input path="lastname"/><br/>
		Email: <form:input path="email" required="required" /><br/>
		phone: <form:input path="phone" required="required" /><br/>
		Birthdate: <form:input path="birthdate"/><br/>
		Gender: 
		<form:select path="gender">
			<form:option value="male">male</form:option>
			<form:option value="female">female</form:option>
		</form:select><br/>

        password: <form:input path="password" required="required"/><br/>
        Re-password: <form:input path="passwordConfirm" required="required"/><br/>
		<input type="reset">
		<button type = "submit">Save</button>
	</form:form>

	<c:forEach items="${errors}" var="item">
    <ul>
		<li style="color: red;">${item}</li>
	</ul>
	</c:forEach>

</body>
</html>